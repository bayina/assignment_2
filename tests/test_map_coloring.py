from src.map_coloring import is_valid, color_state, map_coloring, adjacency_list


def test_is_valid():
    assert is_valid('Red', 'Green') == True
    assert is_valid('Red', 'Red') == False
    assert is_valid('Red', None) == True


def test_color_state():
    coloring = {}
    assert color_state('NY', coloring) == True
    assert coloring['NY'] == 'Red'


def test_map_coloring():
    coloring_result = map_coloring()
    assert coloring_result != {}
    assert len(coloring_result) == len(adjacency_list)
