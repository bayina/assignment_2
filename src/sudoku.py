import os
from typing import List, Tuple, Union


def print_board(board: List[List[Union[int, str]]]) -> None:
    """
    Print the Sudoku board.

    Args:
        board (List[List[Union[int, str]]]): The Sudoku board.

    Returns:
        None
    """
    for i in range(9):
        for j in range(9):
            print(board[i][j], end=" ")
        print()


def find_empty(board: List[List[Union[int, str]]]) -> Union[Tuple[int, int], None]:
    """
    Find the first empty cell (cell with value '.') in the Sudoku board.

    Args:
        board (List[List[Union[int, str]]]): The Sudoku board.

    Returns:
        Union[Tuple[int, int], None]: Row and column indices of the empty cell, or None if not found.
    """
    for i in range(9):
        for j in range(9):
            if board[i][j] == '.':
                return (i, j)
    return None


def is_valid(board: List[List[Union[int, str]]], num: int, pos: Tuple[int, int]) -> bool:
    """
    Check if placing a number in a given position is valid according to Sudoku rules.

    Args:
        board (List[List[Union[int, str]]]): The Sudoku board.
        num (int): The number to be placed.
        pos (Tuple[int, int]): The position (row, column) to be checked.

    Returns:
        bool: True if placement is valid, False otherwise.
    """
    # Check row
    for i in range(9):
        if board[pos[0]][i] == num and pos[1] != i:
            return False

    # Check column
    for i in range(9):
        if board[i][pos[1]] == num and pos[0] != i:
            return False

    # Check subgrid
    box_x = pos[1] // 3
    box_y = pos[0] // 3
    for i in range(box_y*3, box_y*3 + 3):
        for j in range(box_x*3, box_x*3 + 3):
            if board[i][j] == num and (i, j) != pos:
                return False

    return True


def solve_sudoku(board: List[List[Union[int, str]]]) -> bool:
    """
    Solve the Sudoku puzzle using a backtracking algorithm.

    Args:
        board (List[List[Union[int, str]]]): The Sudoku board.

    Returns:
        bool: True if a solution is found, False otherwise.
    """
    empty = find_empty(board)
    if not empty:
        return True
    else:
        row, col = empty

    for i in map(str, range(1, 10)):  # Modified this line to iterate over strings
        if is_valid(board, i, (row, col)):
            board[row][col] = i

            if solve_sudoku(board):
                return True

            board[row][col] = '.'

    return False


def get_user_input() -> List[List[Union[int, str]]]:
    """
    Get user input for the initial Sudoku board.

    Returns:
        List[List[Union[int, str]]]: The Sudoku board.
    """
    board = []
    print("Enter the Sudoku board (row-wise, use . for empty cells):")
    for _ in range(9):
        row = list(input())
        board.append(row)
    return board


if __name__ == "__main__":
    # reading file to get input to run in CI environment
    file_name = "src/input.txt"
    with open(file_name, 'r') as file:
        ci_board = [list(line.strip()) for line in file]
    
    sudoku_board = ci_board if os.getenv("SUDOKU") == "ci" else get_user_input()

    if solve_sudoku(sudoku_board):
        print("\nSolved Sudoku:")
        print_board(sudoku_board)
    else:
        print("\nNo solution exists.")
