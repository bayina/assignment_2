from typing import List, Dict, Optional

adjacency_list: Dict[str, List[str]] = {
    'AL': ['TN', 'GA', 'FL', 'MS'],
    'AR': ['MO', 'TN', 'MS', 'LA', 'TX', 'OK'],
    'AZ': ['CA', 'NV', 'UT', 'NM'],
    'CA': ['OR', 'NV', 'AZ'],
    'CO': ['WY', 'NE', 'KS', 'OK', 'NM', 'UT'],
    'CT': ['NY', 'MA', 'RI'],
    'DC': ['MD', 'VA'],
    'DE': ['MD', 'PA', 'NJ'],
    'FL': ['GA', 'AL'],
    'GA': ['NC', 'SC', 'FL', 'AL', 'TN'],
    'IA': ['MN', 'SD', 'NE', 'MO', 'IL', 'WI'],
    'ID': ['WA', 'OR', 'MT', 'WY', 'UT', 'NV'],
    'IL': ['WI', 'IA', 'MO', 'KY', 'IN'],
    'IN': ['IL', 'KY', 'OH', 'MI'],
    'KS': ['NE', 'CO', 'OK', 'MO'],
    'KY': ['OH', 'IN', 'IL', 'MO', 'TN', 'VA', 'WV'],
    'LA': ['AR', 'MS', 'TX'],
    'MA': ['NY', 'CT', 'RI', 'NH', 'VT'],
    'MD': ['PA', 'DE', 'WV', 'VA', 'DC'],
    'ME': ['NH'],
    'MI': ['IN', 'OH', 'WI'],
    'MN': ['ND', 'SD', 'IA', 'WI'],
    'MO': ['IA', 'NE', 'KS', 'OK', 'AR', 'TN', 'KY', 'IL'],
    'MS': ['TN', 'AR', 'LA', 'AL'],
    'MT': ['ID', 'ND', 'SD', 'WY'],
    'NC': ['VA', 'TN', 'GA', 'SC'],
    'ND': ['MT', 'MN', 'SD'],
    'NE': ['SD', 'WY', 'CO', 'KS', 'MO', 'IA'],
    'NH': ['ME', 'VT', 'MA'],
    'NJ': ['NY', 'DE', 'PA'],
    'NM': ['CO', 'OK', 'TX', 'AZ'],
    'NV': ['OR', 'CA', 'ID', 'UT', 'AZ'],
    'NY': ['VT', 'MA', 'CT', 'NJ', 'PA'],
    'OH': ['MI', 'IN', 'KY', 'WV', 'PA'],
    'OK': ['KS', 'CO', 'NM', 'TX', 'AR', 'MO'],
    'OR': ['WA', 'ID', 'CA', 'NV'],
    'PA': ['NY', 'NJ', 'DE', 'MD', 'WV', 'OH'],
    'RI': ['MA', 'CT'],
    'SC': ['NC', 'GA'],
    'SD': ['ND', 'MT', 'WY', 'NE', 'IA', 'MN'],
    'TN': ['KY', 'MO', 'AR', 'MS', 'AL', 'GA', 'NC', 'VA'],
    'TX': ['NM', 'OK', 'AR', 'LA'],
    'UT': ['ID', 'NV', 'AZ', 'CO', 'WY'],
    'VA': ['WV', 'KY', 'TN', 'NC', 'MD', 'DC'],
    'VT': ['NH', 'NY', 'MA'],
    'WA': ['OR', 'ID'],
    'WI': ['MN', 'IA', 'IL', 'MI'],
    'WV': ['OH', 'KY', 'VA', 'MD', 'PA'],
    'WY': ['MT', 'ID', 'UT', 'CO', 'NE', 'SD']
}


# Define the colors with descriptive names
colors: List[str] = ['Red', 'Green', 'Blue', 'Yellow', 'Cyan', 'Magenta', 'Orange', 'Purple', 'Black']


def is_valid(color: str, neighbor_color: Optional[str]) -> bool:
    """
    Check if it's valid to color a state with a certain color.

    Args:
        color (str): The color to use.
        neighbor_color (str): The color of a neighboring state.

    Returns:
        bool: True if it's valid to color the state with the given color, False otherwise.
    """
    return color != neighbor_color

def color_state(state: str, coloring: Dict[str, str]) -> bool:
    """
    Attempt to color a state.

    Args:
        state (str): The state to be colored.
        coloring (Dict[str, str]): The current coloring of states.

    Returns:
        bool: True if the state was successfully colored, False otherwise.
    """
    for color in colors:
        if all(is_valid(color, coloring.get(neighbor, None)) for neighbor in adjacency_list[state]):
            coloring[state] = color
            return True
    return False


def map_coloring() -> Dict[str, str]:
    """
    Perform the map coloring algorithm.

    Returns:
        Dict[str, str]: A dictionary mapping states to their colors, or an empty dictionary if coloring fails.
    """
    coloring: Dict[str, str] = {}
    for state in sorted(adjacency_list.keys()):
        if state in coloring:
            continue
        if not color_state(state, coloring):
            return {}  # Return an empty dictionary if coloring fails
    return coloring


if __name__ == "__main__":
    coloring_result = map_coloring()
    if coloring_result:
        print("Map coloring successful!")
        print("Final mapping:")
        for state, color in sorted(coloring_result.items()):
            print(f"{state}: {color}")
    else:
        print("Map coloring failed. No valid coloring found.")
