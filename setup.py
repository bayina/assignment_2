from setuptools import setup, find_packages

setup(
    name='assignment_2',
    version='1.0.0',
    description='sudoku and map coloring',
    author='Chandhini Bayina',
    author_email='chansmarupilla@outlook.com',
    packages=find_packages(),
    install_requires=[
        'pytest'
    ]
)
